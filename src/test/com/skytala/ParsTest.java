package test.com.skytala;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import main.com.skytala.Pars;

public class ParsTest {
	private static String path;

	@Test
	public void checkParsAnything() {
		Pars mypars = new Pars();
		String testString = "type=" + '"' + "hallo" + '"';
		String testString2 = "type  =   " + '"' + "hallo" + '"';
		String testString3 = "type=" + '"' + "123*ahsdfl�a" + '"';

		assertEquals("ParsAnything must be hallo", "hallo", mypars.parsanything(testString, "type"));
		assertEquals("ParsAnything must be hallo", "hallo", mypars.parsanything(testString2, "type"));
		assertEquals("ParsAnything must be hallo", "123*ahsdfl�a", mypars.parsanything(testString3, "type"));
	}

	@Test
	public void testConvert() {
		Pars mypars = new Pars();
		String testString = "id-ne";
		String testString2 = "fjdsal";

		assertEquals("Convert must me String", "String", mypars.convertEntityTypeToJava(testString));
		assertEquals("Convert must be nothing", "", mypars.convertEntityTypeToJava(testString2));

	}

	@Test
	public void testgetAllServicesandAttributes() {

		FileReader newfr;
		FileReader oldfr;
		BufferedReader newbr;
		BufferedReader oldbr;
		String oldLine;
		String newLine;

		try {
			oldfr = new FileReader(path+"/unittest/services.txt");
			newfr = new FileReader(path+"/outputParser/services.txt");

			newbr = new BufferedReader(newfr);
			oldbr = new BufferedReader(oldfr);

			while ((newLine = newbr.readLine()) != null) {
				oldLine = oldbr.readLine();
				assertEquals("The Lines must have the same content", oldLine, newLine);

			}
			oldbr.close();
			newbr.close();
			oldfr.close();
			newfr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testparsParsedServices() {

		FileReader newfr;
		FileReader oldfr;
		BufferedReader newbr;
		BufferedReader oldbr;
		String oldLine;
		String newLine;

		try {
			oldfr = new FileReader(path+"/unittest/parsedParsed.txt");
			newfr = new FileReader(path+"/outputParser/parsedParsed.txt");

			newbr = new BufferedReader(newfr);
			oldbr = new BufferedReader(oldfr);

			while ((newLine = newbr.readLine()) != null) {
				oldLine = oldbr.readLine();
				assertEquals("The Lines must have the same content", oldLine, newLine);

			}
			oldbr.close();
			newbr.close();
			oldfr.close();
			newfr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testparsOverride() {

		FileReader newfr;
		FileReader oldfr;
		BufferedReader newbr;
		BufferedReader oldbr;
		String oldLine;
		String newLine;

		try {
			oldfr = new FileReader(path+"/unittest/final.txt");
			newfr = new FileReader(path+"/outputParser/final.txt");

			newbr = new BufferedReader(newfr);
			oldbr = new BufferedReader(oldfr);

			while ((newLine = newbr.readLine()) != null) {
				oldLine = oldbr.readLine();
				assertEquals("The Lines must have the same content", oldLine, newLine);

			}
			oldbr.close();
			newbr.close();
			oldfr.close();
			newfr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testeliminateOut() {

		FileReader newfr;
		FileReader oldfr;
		BufferedReader newbr;
		BufferedReader oldbr;
		String oldLine;
		String newLine;

		try {
			oldfr = new FileReader(path+"/unittest/parsedServices.txt");
			newfr = new FileReader(path+"/outputParser/parsedServices.txt");

			newbr = new BufferedReader(newfr);
			oldbr = new BufferedReader(oldfr);

			while ((newLine = newbr.readLine()) != null) {
				oldLine = oldbr.readLine();
				assertEquals("The Lines must have the same content", oldLine, newLine);

			}
			oldbr.close();
			newbr.close();
			oldfr.close();
			newfr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testeliminateDuplicates() {
		FileReader newfr;
		FileReader oldfr;
		BufferedReader newbr;
		BufferedReader oldbr;
		String oldLine;
		String newLine;

		try {
			oldfr = new FileReader(path+"/unittest/noduplicates.txt");
			newfr = new FileReader(path+"/outputParser/noduplicates.txt");

			newbr = new BufferedReader(newfr);
			oldbr = new BufferedReader(oldfr);

			while ((newLine = newbr.readLine()) != null) {
				oldLine = oldbr.readLine();
				assertEquals("The Lines must have the same content", oldLine, newLine);

			}
			oldbr.close();
			newbr.close();
			oldfr.close();
			newfr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testSplit(){
		String oldfile = path+"/unittest/Splitted/";
		String newfile = path+"/outputParser/parsed/";
		BufferedReader oldbr;
		BufferedReader newbr;
		FileReader oldfr;
		FileReader newfr;
		String oldLine;
		String newLine;
		
		for(int i =0 ; i<=117;i++){
			try {
				oldfile = path+"/unittest/Splitted/"+i+".txt";
				newfile = path+"/outputParser/parsed/"+i+".txt";
				oldfr = new FileReader (oldfile);
				newfr = new FileReader(newfile);
				oldbr = new BufferedReader(oldfr);
				newbr = new BufferedReader(newfr);
				
				while((oldLine = oldbr.readLine())!=null){
					newLine = newbr.readLine();
					
					assertEquals("The Lines must have the same content", oldLine, newLine);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
		}
	}
	
	@BeforeClass
	public static void prepareTests() {
		path = new File("").getAbsolutePath();
		
		Pars mypars = new Pars();
		mypars.initPath();
		mypars.getAllServicesAndAttributes(
				new File(path+"/GITXML/miscellaneous/service_xmls/"));
		mypars.parsParsedServices();
		mypars.parsOverride();
		mypars.eliminateOut();
		mypars.eliminateDuplicates();
		mypars.split();

	}
}
