  <description>Fedex Shipment Interface Services, based on Ship Manager Direct XML API</description>
Servicename: "fedexSubscriptionRequest"
Attr: "contactPartyName"  type:"String"  optional:"false"
Attr: "companyPartyId"  type:"String"  optional:"false"
Attr: "replaceMeterNumber"  type:"Boolean"  optional:"false"
Attr: "shipmentGatewayConfigId"  type:"String"  optional:"false"
Attr: "configProps"  type:"String"  optional:"false"


Servicename: "fedexShipRequest"
Attr: "shipmentId"  type:"String"  optional:"false"
Attr: "shipmentRouteSegmentId"  type:"String"  optional:"false"


